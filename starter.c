#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    // The data you want to store in the node
    int value;
    struct node * next;
} Node;

// write the code to print out the list
void printList(Node * head) {
    printf("-----------\n");
    Node * curr = head;
    while (curr != NULL) {
        printf("List item: %d\n", curr->value);
        curr = curr->next;
    }
    printf("Done printing list.\n");
}

// count()
// - output the total number of items in the list
void count(Node * head) {
    printf("-----------\n");
    int count = 0;
    Node * curr = head;
    while (curr != NULL) {
        count = count + 1;
        curr = curr->next;
    }
    printf("Total number of items: %d\n", count);
}

// EXERCISE 1: Create the contains() function
// contains() should accept the following parameters:
//  - pointer to the front of the list
//  - value to search for
// If value is in the list, return true (1) & output "Found!" to screen
// Else, return false (0) & output "Not found!" to screen
int contains(Node * head, int valueToSearchFor) {
    //@TODO: Write code for this function
    Node * curr = head;
    while (curr != NULL) {
        if (curr->value == valueToSearchFor) {
            printf("FOUND! %d is in the list\n", valueToSearchFor);
            return 1;
        }
        curr = curr->next;
    }
    
    printf("NOT FOUND! %d is not in the list\n", valueToSearchFor);
    return 0;
}

Node * createNode(int value) {
    // this function should accept a value
    // create a new node based on that value
    
    // hey c, go make me a space in memory to store a Node struct
    // And don't get rid of it after you exit the function
    // malloc = memory allocation
    // CONS:  YOU have to manually get rid of this memory spae
    // when you are done with the variable
    Node * n = malloc(sizeof(Node));
    n->value = value;
    n->next = NULL;
    return n;
    
    //    n.value = value;
    //    n.next  = NULL;
    
    // return the address of that new value
    //    return &n;
}

Node * insertAtHead(Node * currentHead, Node * nodeToInsert) {
    // we are making brandon grab onto jenelle
    // At the end of this:  brandon is grabbign jenelle, jenelle is grabbgin nabil
    nodeToInsert->next = currentHead;
    return nodeToInsert;
}

void insertAtTail(Node * curr, Node * nodeToInsert) {
    // find the last item in the list
    while (curr->next != NULL) {
        curr = curr->next;
    }
    // once you are at the last item, set the NEXT pointer to be the new node
    curr->next = nodeToInsert;
}

void deleteAtHead(Node * head) {
    // 1. set the new head of the list to head->next
    // 2. delete the head
}

void deleteAtTail(Node * head) {
    // 1. iterate to the second last item in the list
    // 2. set the "next" to be NULL
}

void deleteAfter(Node * n1) {
    // delete the node that comes after n1
}
// insert n2 AFTER n1
// in our current list:  jenelle - stephen - nabil
// I want to insert chris after jenelle
// Expected output: jenelle  - chris - stephen - nabil
// For this example: n1 = jenelle, n2 = chris
// *** REMEMBER THAT JENELLE IS CURRENTLY GRABBING STEPHEN!
void insertAfter(Node * n1, Node * n2) {
    // set n2-> next = n1->next
    // set n1->next = n2
    n2->next = n1->next;         // chris grabs stephen
    n1->next = n2;               // jenelle grabs chris
    
    // the final list should be: jenelle - chris - stephen
}



int main() {
    // Optimization:  Create a "head" variable
    // This variable keeps track of where the front of the list is
    Node * head;
    
    Node n1, n2, n3;
    // set the values of the nodes
    n1.value = 99;
    n2.value = 100;
    n3.value = 101;
    
    // setup the relationships between the nodes
    n1.next = &n2;
    n2.next = &n3;
    n3.next = NULL;
    
    Node * n4 = createNode(102);
    n3.next = n4;
    
    // change head to brandon
    // Now, when you change the front of the list, you can also just reset the
    // head pointer
    Node * brandon = createNode(42);
    head = insertAtHead(&n1, brandon);
    
    // change head to stephen
    Node * stephen = createNode(19);
    head = insertAtHead(brandon, stephen);     //stephen becomes the head
    
    // change head to stephen
    Node * tavin = createNode(6);
    head = insertAtHead(stephen,tavin);
    
    // change tail to chris
    Node * chris = createNode(7);
    insertAtTail(head, chris);
    
    // Example of inserting someone INBETWEEN another node
    Node * jharna = createNode(2);
    insertAfter(&n1, jharna);
    
    // go print the list --
    // beacuse you have a "head" variable, you can just call
    // the below function with "head", instead of manually udpating it to
    // brandon, stephen, or tavin.
    printList(head);
    count(head);         // expected output 5
    
    return 0;
}

